import 'dart:convert';
import 'package:rocket_lab_exam/networking/implementations/endpoints/post_login/login_success_response.dart';
import 'package:rocket_lab_exam/networking/interfaces/endpoint.dart';

final class PostLoginEndpoint implements Endpoint<LoginSuccessResponse> {
  final String email;
  final String password;

  const PostLoginEndpoint({required this.email, required this.password});

  @override
  Map<String, dynamic> get body => {
        'authLogin': {
          'email': email,
          'pass': password,
        }
      };

  @override
  LoginSuccessResponse decode(String data) {
    final json = jsonDecode(data);
    final Map<String, dynamic> authLogin = json['authLogin'];
    final firstName = authLogin['firstname'];
    final token = authLogin['token'];
    final refreshToken = authLogin['refreshToken'];

    if (firstName != null && token != null && refreshToken != null) {
      return LoginSuccessResponse(
        firstName: firstName,
        token: token,
        refreshToken: refreshToken,
      );
    } else {
      return const LoginSuccessResponse(
        firstName: "",
        token: "",
        refreshToken: "",
      );
    }
  }

  @override
  Map<String, String> get headers => {
        'Content-Type': 'application/json',
        'x-api-key': 'c37861c7-7414-4a40-bbd8-3343662e4483',
      };

  @override
  String get host => 'testvm1.rokt.io';

  @override
  String get method => 'POST';

  @override
  String get path => '/api/jsonql';

  @override
  String get scheme => 'https';
}
