import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
final class LoginSuccessResponse {
  final String firstName;
  final String token;
  final String refreshToken;

  const LoginSuccessResponse({
    required this.firstName,
    required this.token,
    required this.refreshToken,
  });
}
