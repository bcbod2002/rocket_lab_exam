import 'dart:convert';

import 'package:http/http.dart';
import 'package:rocket_lab_exam/networking/interfaces/endpoint.dart';
import 'package:rocket_lab_exam/networking/interfaces/network_client.dart';

final class AppClient implements NetworkClient {
  final Client _client;

  AppClient({required Client client}) : _client = client;

  @override
  void cancel() {
    _client.close();
  }

  @override
  Future<DataType> send<DataType, E extends Endpoint<DataType>>(
      E endpoint) async {
    final request = Request(
      endpoint.method,
      Uri(
        scheme: endpoint.scheme,
        host: endpoint.host,
        path: endpoint.path,
      ),
    );

    request.body = jsonEncode(endpoint.body);
    request.headers.addAll(endpoint.headers);
    final result = await Response.fromStream(await _client.send(request));

    return endpoint.decode(result.body);
  }
}
