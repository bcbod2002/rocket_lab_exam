abstract class Endpoint<DataType> {
  String get scheme;
  String get host;
  String get method;
  String get path;
  Map<String, String> get headers;
  Map<String, dynamic> get body;

  DataType decode(String data);
}
