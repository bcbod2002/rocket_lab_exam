import 'dart:convert';

import 'package:rocket_lab_exam/networking/interfaces/endpoint.dart';
import 'package:http/http.dart';

abstract class NetworkClient {
  Future<DataType> send<DataType, E extends Endpoint<DataType>>(E endpoint);

  void cancel();
}
