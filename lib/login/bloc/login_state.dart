part of 'login_bloc.dart';

sealed class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

final class LoginInitial extends LoginState {}

final class LoginSuccessState extends LoginState {
  final String name;
  final String token;
  final String refreshToken;

  const LoginSuccessState({
    required this.name,
    required this.token,
    required this.refreshToken,
  });
}
