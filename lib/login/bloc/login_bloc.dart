import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rocket_lab_exam/networking/implementations/endpoints/post_login/login_success_response.dart';
import 'package:rocket_lab_exam/networking/implementations/endpoints/post_login/post_login_endpoint.dart';
import 'package:rocket_lab_exam/networking/interfaces/network_client.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final NetworkClient _client;

  LoginBloc(NetworkClient client)
      : _client = client,
        super(LoginInitial()) {
    on<SendLoginEvent>(_login);
  }

  Future<void> _login(
    SendLoginEvent event,
    Emitter<LoginState> state,
  ) async {
    final result = await _client.send<LoginSuccessResponse, PostLoginEndpoint>(
      const PostLoginEndpoint(
        email: 'js+dev8@rokt.io',
        password: 'Helloworld',
      ),
    );

    final state = LoginSuccessState(
      name: result.firstName,
      token: result.token,
      refreshToken: result.refreshToken,
    );

    emit(state);
  }
}
