import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:rocket_lab_exam/item_list/item_list_page.dart';
import 'package:rocket_lab_exam/login/bloc/login_bloc.dart';
import 'package:rocket_lab_exam/networking/implementations/app_client.dart';

class LoginPage extends StatelessWidget {
  LoginPage({super.key});

  String _userName = "";
  String _password = "";

  final LoginBloc _bloc = LoginBloc(AppClient(client: Client()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Login"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("User Name"),
              SizedBox(
                width: 200,
                child: TextField(
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Enter your user name"),
                  onChanged: (text) {
                    _userName = text;
                  },
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Password"),
              SizedBox(
                width: 200,
                child: TextField(
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Enter your password"),
                  // obscureText: true,
                  enableSuggestions: false,
                  autocorrect: false,
                  onChanged: (text) {
                    _password = text;
                  },
                ),
              ),
            ],
          ),
          BlocListener(
            listener: (context, state) {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return ItemListPage();
              }));
            },
            bloc: _bloc,
            child: TextButton(
              onPressed: () {
                final event = SendLoginEvent(
                  email: _userName,
                  password: _password,
                );
                _bloc.add(event);
              },
              child: const Text("Login"),
            ),
          ),
        ],
      ),
    );
  }
}
