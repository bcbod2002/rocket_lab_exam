import 'package:flutter/material.dart';

class ItemListPage extends StatelessWidget {
  const ItemListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Item List"),
      ),
      body: Center(
        child: ListView.builder(
          itemCount: 3,
          itemBuilder: (BuildContext context, int index) {
            return const _ItemView(id: "1", title: "1", description: "1");
          },
        ),
      ),
    );
  }
}

class _ItemView extends StatelessWidget {
  final String _id;
  final String _title;
  final String _description;

  const _ItemView({
    super.key,
    required String id,
    required String title,
    required String description,
  })  : _id = id,
        _title = title,
        _description = description;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(child: Text(_id)),
        Expanded(child: Text(_title)),
        Expanded(child: Text(_description)),
      ],
    );
  }
}
